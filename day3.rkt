#lang racket
(require racket/base)
(define lines (call-with-input-file* "input3_1" port->lines #:mode 'text))
(define number-positions
  (map (lambda (x i) (cons i
                           (map (lambda (y) (list (list (car y) (cdr y))
                                                  (string->number (substring x (car y) (cdr y)))))
                                (regexp-match-positions* #rx"[0-9]+" x))))
       lines (range (length lines))))
(define symbol-positions
  (map (lambda (x i) (cons i (map car (regexp-match-positions* #rx"[^0-9.]" x))))
       lines (range (length lines))))
(define gear-positions
  (map (lambda (x i) (cons i (map car (regexp-match-positions* #rx"\\*" x))))
       lines (range (length lines))))
(define (filter-part-numbers p sp)
  (append* (map (lambda (line-symbols)
                  (map (lambda (symbol)
                         (let ((numbers (append* (map (lambda (rel-index) (dict-ref p (- (car line-symbols) rel-index) '()))
                                                      '(-1 0 1)))))
                           (filter (lambda (number) (and (>= symbol (sub1 (caar number)))
                                                         (<= symbol (cadar number)))) numbers))) (cdr line-symbols))) sp)))

(define (part1) (foldr + 0 (map cadr (append* (filter-part-numbers number-positions symbol-positions)))))
(define (part2) (foldr + 0 (filter-map (lambda (parts) 
                                         (if (= 2 (length parts))
                                             (foldr * 1 (map cadr parts))
                                             #f)) (filter-part-numbers number-positions gear-positions))))
(part1)
(part2)